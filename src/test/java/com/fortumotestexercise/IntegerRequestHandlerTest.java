package com.fortumotestexercise;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IntegerRequestHandlerTest {

  private static final long SLEEP = 250L;

  private final List<BigInteger> requestResults;
  private final List<Thread> threads;
  private IntegerRequestHandler requestHandler;
  private CyclicBarrier gate;

  public IntegerRequestHandlerTest() {
    requestResults = Collections.synchronizedList(new ArrayList<>());
    threads = new ArrayList<>();
  }

  @Before
  public void setUp() {
    requestHandler = new IntegerRequestHandler();
    requestResults.clear();
    threads.clear();
  }

  @Test
  public void addSingleInteger() throws Exception {
    gate = new CyclicBarrier(2);
    createThreads(10L, 1);
    threads.forEach(Thread::start);
    gate.await();
    Thread.sleep(SLEEP);

    requestResults.add(requestHandler.handleTerminatingRequest());
    assertTrue(requestResults.stream().allMatch(integer -> integer.equals(BigInteger.valueOf(10L))));
  }

  @Test
  public void add20IntegersConcurrently() throws Exception {
    gate = new CyclicBarrier(21);
    createThreads(10L, 20);
    threads.forEach(Thread::start);
    gate.await();
    Thread.sleep(SLEEP);

    requestResults.add(requestHandler.handleTerminatingRequest());
    assertTrue(requestResults.stream().allMatch(integer -> integer.equals(BigInteger.valueOf(200L))));
  }

  @Test
  public void add100IntegersConcurrently() throws Exception {
    gate = new CyclicBarrier(101);
    createThreads(10L, 100);
    threads.forEach(Thread::start);
    gate.await();
    Thread.sleep(SLEEP);

    requestResults.add(requestHandler.handleTerminatingRequest());
    assertTrue(requestResults.stream().allMatch(integer -> integer.equals(BigInteger.valueOf(1000L))));
  }

  @Test
  public void addIntegersThatSumTo10BillionConcurrently() throws Exception {
    gate = new CyclicBarrier(101);
    createThreads(100000000L, 100);
    threads.forEach(Thread::start);
    gate.await();
    Thread.sleep(SLEEP);

    requestResults.add(requestHandler.handleTerminatingRequest());
    assertTrue(requestResults.stream().allMatch(integer -> integer.equals(BigInteger.valueOf(10000000000L))));
  }

  @Test
  public void integersAreResetCorrectlyAfterTerminatingRequest() throws Exception {
    gate = new CyclicBarrier(2);
    createThreads(10L, 1);
    threads.forEach(Thread::start);
    gate.await();
    Thread.sleep(SLEEP);

    requestHandler.handleTerminatingRequest();
    assertEquals(requestHandler.handleTerminatingRequest(), BigInteger.ZERO);
  }

  @Test
  public void singleTerminatingRequestReturnsZero() {
    assertEquals(requestHandler.handleTerminatingRequest(), BigInteger.ZERO);
  }

  private void createThreads(long integerToAdd, int nrOfThreads) {
    for (int i = 0; i < nrOfThreads; i++) {
      Thread thread = new Thread(() -> {
        try {
          gate.await();
          requestResults.add(requestHandler.handleIntegerRequest(integerToAdd));
        } catch (Exception ex) {
          return;
        }
      });
      threads.add(thread);
    }
  }

}