package com.fortumotestexercise;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.stream.Collectors;

@WebServlet(name = "IntegerServlet", urlPatterns = {""}, loadOnStartup = 1)
public class HttpIntegerServlet extends HttpServlet {

  private static final String TERMINATOR = "END";
  private final IntegerRequestHandler requestHandler = new IntegerRequestHandler();

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String requestData = getRequestData(request);
    BigInteger sum;
    if (TERMINATOR.equalsIgnoreCase(requestData)) {
      sum = requestHandler.handleTerminatingRequest();
    } else if (isNumeric(requestData)) {
      sum = requestHandler.handleIntegerRequest(Long.parseLong(requestData));
    } else {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      return;
    }

    createSumResponse(response, sum);
  }

  private String getRequestData(HttpServletRequest request) throws IOException {
    return request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
  }

  private boolean isNumeric(String value) {
    if (value == null) {
      return false;
    }

    try {
      Long.parseLong(value);
    } catch (NumberFormatException ex) {
      return false;
    }

    return true;
  }

  private void createSumResponse(HttpServletResponse response, BigInteger sum) {
    try {
      response.getWriter().print(sum.toString());
    } catch (IOException ex) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
  }

}
